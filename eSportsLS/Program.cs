﻿using System;

namespace eSportsLS
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = args[0];
            Menu menu = new Menu();
            DataManager program = new DataManager();
            program.readJSON(filename);

            do
            {
                menu.showMenuMethods();
                menu.optionInput1();
                //Si la opción es valida y no es la de salir, entramos
                if (menu.validOption1() && !menu.isExit())
                {
                    do
                    {
                        menu.showMenuFunctionality(menu.getOption1());
                        menu.optionInput2();
                    } while (!menu.validOption2(menu.getOption1()));
                    program.start(menu.getOption1(), menu.getOption2());
                }
            } while (!menu.validOption1() || !menu.isExit());
        }
    }
}
