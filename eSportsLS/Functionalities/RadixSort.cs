﻿using System;
using System.Collections;
using System.Collections.Generic;
using static eSportsLS.DataManager;

namespace eSportsLS.Functionalities
{
    class RadixSort
    {
        const int NUM_ALPHABET = 27 + 10 + 1; //Alphabet + Numeros + ' '
        const int NUM_DIGITS = 10;
        public eSports[] SortWinrate(eSports[] arr, List<ArrayList> buckets, int posDigit = 0)
        {
            // Cas trivial, it's already sorted  
            if (IsSorted(arr, buckets))
            {

                return arr;
            }
            else // Cas no trivial, anirem posant als buckets dependent del digit
            {
                // Netegem els buckets
                foreach (ArrayList bucket in buckets)
                {
                    bucket.Clear();
                }

                // Per cada team, hem de mirar el digit i el coloquem al bucket corresponent
                for (int i = 0; i < arr.Length; i++)
                {
                    int digit = GetDigit(arr[i].winrate, posDigit);
                    buckets[digit].Add(arr[i]);
                }


                // Reconstruim l'array de teams
                int i_arr = 0;
                int numBuckets = buckets.Count;
                for (int i = numBuckets - 1; i >= 0; i--)
                {
                    int numItems = buckets[i].Count;
                    ArrayList bucket = buckets[i];
                    for (int j = 0; j < numItems; j++)
                    {
                        arr[i_arr++] = (eSports)bucket[j];
                    }
                }

                return SortWinrate(arr, buckets, ++posDigit);
            }
        }

        public List<ArrayList> InitBuckets()
        {
            List<ArrayList> groups = new List<ArrayList>();
            for (int i = 0; i < NUM_DIGITS; i++)
            {
                ArrayList teams = new ArrayList();
                groups.Add(teams);
            }
            return groups;
        }

        private int GetDigit(float winrate, int posDigit)
        {
            double digit = (winrate * 100) / Math.Pow(10, posDigit);
            digit = digit % 10;
            return (int)digit;
        }

        private bool IsSorted(eSports[] arr, List<ArrayList> buckets)
        {

            for (int i = 0; i < buckets.Count; i++)
            {
                if (buckets[i].Count == arr.Length)
                {
                    return true;
                }
            }

            return false;
        }

        public List<List<Element>> InitBucketsElements()
        {
            List<List<Element>> buckets = new List<List<Element>>();
            for (int i = 0; i < NUM_ALPHABET; i++)
            {
                List<Element> elems = new List<Element>();
                buckets.Add(elems);
            }
            return buckets;
        }

        public Element[] SortPriorities(Element[] arr, List<List<Element>> buckets, int posDigit = 0, bool sorted = false)
        {
            // Cas trivial, it's already sorted  
            if (IsSorted(arr, buckets))
            {
                return arr;
            }
            else // Cas no trivial, anirem posant als buckets dependent del digit
            {
                // Netegem els buckets
                foreach (List<Element> bucket in buckets)
                {
                    bucket.Clear();
                }
                // Per cada team, hem de mirar el digit i el coloquem al bucket corresponent
                for (int i = 0; i < arr.Length; i++)
                {
                    int digit = GetBucketLetter(arr[i].jugador.nationality, posDigit);
                    buckets[digit].Add(arr[i]);
                }

                // Reconstruim l'array
                int i_arr = 0;
                int numBuckets = buckets.Count;
                for (int i = numBuckets - 1; i >= 0; i--)
                {
                    int numItems = buckets[i].Count;
                    buckets[i].Sort((x, y) =>
                        x.jugador.nationality.CompareTo(y.jugador.nationality) == 0 ?
                        sameTeamWinrate(x.teamWinrate, y.teamWinrate) == 0 ?
                        biggerKda(x.kda, y.kda) : sameTeamWinrate(x.teamWinrate, y.teamWinrate)
                        : x.jugador.nationality.CompareTo(y.jugador.nationality));
                    List<Element> bucket = buckets[i];
                    for (int j = 0; j < numItems; j++)
                    {
                        arr[i_arr++] = bucket[j];
                    }
                }
                return SortPriorities(arr, buckets, ++posDigit, sorted);
            }
        }

        private bool IsSorted(Element[] arr, List<List<Element>> buckets)
        {

            for (int i = 0; i < buckets.Count; i++)
            {
                if (buckets[i].Count == arr.Length)
                {
                    return true;
                }
            }

            return false;
        }

        private int GetBucketLetter(string nationality, int posLetter)
        {
            string national = nationality.ToLower();

            if (posLetter < national.Length)
            {
                if (national[national.Length - posLetter - 1] == 'ñ') return 'n' - 'a';
                else if (national[national.Length - posLetter - 1] == ' ') return NUM_ALPHABET - 1;
                else if (national[national.Length - posLetter - 1] >= '0' && national[national.Length - posLetter - 1] <= '9')
                    return NUM_ALPHABET - 1 - (national[national.Length - posLetter - 1] - '0');
                else return national[national.Length - posLetter - 1] - 'a';
            }
            else
            {
                return 0;
            }
        }

        private static int sameTeamWinrate(float a1, float a2)
        {
            if (a1 == a2)
            {
                return 0;
            }
            else if (a1 < a2)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        private static int biggerKda(double a1, double a2)
        {
            if (a1 == a2)
            {
                return 0;
            }
            else if (a1 < a2)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        // Otra implementación no recursiva
        public eSports[] SortWinrate(eSports[] arr)
        {
            // Array auxiliares
            long[] t = new long[arr.Length];
            long[] a = new long[arr.Length];
            eSports[] temp = new eSports[arr.Length];
            eSports[] converted = new eSports[arr.Length];
            int lengthArray = arr.Length;

            for (int i = 0; i < lengthArray; i++)
            {
                // Convert float to long
                a[i] = Convert.ToInt64(arr[i].winrate);
                converted[i] = arr[i];
            }

            int setLength = 4;
            int numBits = 64;
            long[] counter = new long[1 << setLength];
            long[] prefixes = new long[1 << setLength];
            int groups = numBits / setLength;
            int mask = (1 << setLength) - 1;
            int negNumbers = 0, posNumbers = 0;

            for (int k = 0, shift = 0; k < groups; k++, shift += setLength)
            {
                // Inicializar el array de contadores
                int lenCounter = counter.Length;
                for (int j = 0; j < lenCounter; j++)
                    counter[j] = 0;

                int lenA = a.Length;
                for (int i = 0; i < lenA; i++)
                {
                    counter[(a[i] >> shift) & mask]++;

                    // Contar numeros negativos
                    if (k == 0 && a[i] < 0)
                        negNumbers++;
                }
                if (k == 0) posNumbers = lenA - negNumbers;

                // Calcular los prefijos
                prefixes[0] = 0;
                for (int i = 1; i < counter.Length; i++)
                    prefixes[i] = prefixes[i - 1] + counter[i - 1];

                for (int i = 0; i < a.Length; i++)
                {
                    // Calcular el indice donde se tiene que ordenar el numero 
                    long indice = prefixes[(a[i] >> shift) & mask]++;

                    if (k == groups - 1)
                    {
                        // MSB. Si es negativo, hacerlo al reves
                        // Hay que poner los positivos despues de los negativos
                        if (a[i] < 0)
                            indice = posNumbers - (indice - negNumbers) - 1;
                        else
                            indice += negNumbers;
                    }
                    t[indice] = a[i];
                    temp[indice] = converted[i];
                }


                t.CopyTo(a, 0);
                temp.CopyTo(converted, 0);
            }

            return converted;
        }

    //----------------- Display Functions ------------------

    public void DisplaySortedWinrates(eSports[] e)
        {
            for (int i = 0; i < e.Length; i++)
            {
                Console.WriteLine(e[i].team + "-" + e[i].winrate);
            }
            Console.WriteLine();
        }
        public void DisplaySortedPlayers(Element[] j)
        {
            foreach (Element aux in j)
            {
                Console.WriteLine(aux.jugador.nationality + " - " + aux.teamWinrate + " - " + aux.kda);
            }

        }
    }
}
