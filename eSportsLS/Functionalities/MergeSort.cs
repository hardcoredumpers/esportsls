﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static eSportsLS.DataManager;

namespace eSportsLS.Functionalities
{
    class MergeSort
    {
        
        public eSports[] mergeSort(eSports[] e, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                mergeSort(e, left, mid);
                mergeSort(e, (mid + 1), right);
                doMerge(e, left, mid + 1, right);
            }
            return e;
        }

        public static void doMerge(eSports[] e, int left, int mid, int right)
        {
            eSports[] tmp = new eSports[e.Length];
            int i, leftEnd, cantElements, posTmp;
            leftEnd = mid - 1;
            posTmp = left;
            cantElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (e[left].winrate >= e[mid].winrate) { tmp[posTmp++] = e[left++]; }
                else { tmp[posTmp++] = e[mid++]; }
            }
            while (left <= leftEnd) { tmp[posTmp++] = e[left++]; }
            while (mid <= right) { tmp[posTmp++] = e[mid++]; }
            for (i = 0; i < cantElements; i++) 
            { 
                e[right] = tmp[right]; 
                right--; 
            }
        }

        public Jugador[] mergeSortNationality(Jugador[] j, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                mergeSortNationality(j, left, mid);
                mergeSortNationality(j, (mid + 1), right);
                doMergeNationality(j, left, mid + 1, right);
            }
            return j;

        }
        public static void doMergeNationality(Jugador[] j, int left, int mid, int right)
        {
            Jugador[] tmp = new Jugador[j.Length];
            int i, leftEnd, cantElements, posTmp;
            leftEnd = mid - 1;
            posTmp = left;
            cantElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (j[left].nationality.CompareTo(j[mid].nationality) < 0) 
                { 
                    tmp[posTmp++] = j[left++]; 
                }
                else if (j[left].nationality.CompareTo(j[mid].nationality) == 0)
                {
                    if (j[left].name.CompareTo(j[mid].name) < 0)
                    {
                        tmp[posTmp++] = j[left++];
                    }
                    else
                    {
                        tmp[posTmp++] = j[mid++];
                    }
                }
                else {            
                    tmp[posTmp++] = j[mid++]; 
                }   
            }
            while (left <= leftEnd) { tmp[posTmp++] = j[left++]; }
            while (mid <= right) { tmp[posTmp++] = j[mid++]; }
            for (i = 0; i < cantElements; i++)
            {
                j[right] = tmp[right];
                right--;
            }
           
        }

        public Element[] mergeSortAux(Element[] e, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                mergeSortAux(e, left, mid);
                mergeSortAux(e, (mid + 1), right);
                doMergeAux(e, left, mid + 1, right);

            }
            return e;
        }

        public void doMergeAux (Element[] e, int left, int mid, int right)
        {
            Element[] tmp = new Element[e.Length];
            int i, leftEnd, cantElements, posTmp;
            leftEnd = mid - 1;
            posTmp = left;
            cantElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                if (e[left].jugador.nationality.CompareTo(e[mid].jugador.nationality) < 0) { tmp[posTmp++] = e[left++]; }
                else { tmp[posTmp++] = e[mid++]; }
            }
            while (left <= leftEnd) { tmp[posTmp++] = e[left++]; }
            while (mid <= right) { tmp[posTmp++] = e[mid++]; }
            for (i = 0; i < cantElements; i++)
            {
                e[right] = tmp[right];
                right--;
            }

        }
        
        public Element[] mergeSortPlayers (Element[] jugadores, int left, int right)
        {
            int mid;
            if (right > left)
            {
                mid = (right + left) / 2;
                mergeSortPlayers(jugadores, left, mid);
                mergeSortPlayers(jugadores, (mid + 1), right);
                doMergePlayers(jugadores, left, mid + 1, right);
            }
            return jugadores;
        }

        public static void doMergePlayers(Element[] e, int left, int mid, int right)
        {
            Element [] tmp = new Element [e.Length];
            int i, leftEnd, cantElements, posTmp;
            leftEnd = mid - 1;
            posTmp = left;
            cantElements = (right - left + 1);

            while ((left <= leftEnd) && (mid <= right))
            {
                //Si la nacionalidad es diferente
                if (e[left].jugador.nationality.CompareTo(e[mid].jugador.nationality) < 0)
                {
                    tmp[posTmp++] = e[left++];
                }
                //Si la nacionalidad es igual, miramos el winrate luego el kda
                else if (e[left].jugador.nationality.CompareTo(e[mid].jugador.nationality) == 0)
                {
                    if (e[left].teamWinrate > e[mid].teamWinrate)
                    {
                        tmp[posTmp++] = e[left++];
                    }
                    else if (e[left].teamWinrate == e[mid].teamWinrate)
                    {
                        if (e[left].kda >= e[mid].kda)
                        {
                            tmp[posTmp++] = e[left++];
                        }
                        else
                        {
                            tmp[posTmp++] = e[mid++];
                        }
                    }
                    else
                    {
                        tmp[posTmp++] = e[mid++];
                    }
                }
                else
                {
                    tmp[posTmp++] = e[mid++];
                }
            }
            while (left <= leftEnd) { tmp[posTmp++] = e[left++]; }
            while (mid <= right) { tmp[posTmp++] = e[mid++]; }
            for (i = 0; i < cantElements; i++)
            {
                e[right] = tmp[right];
                right--;
            }
        }
        
        /*
         * Display functions 
         */
        public void display(eSports[] e)
        {
            foreach (eSports aux in e)
            {
                Console.WriteLine("Team Name: " + aux.team + " - " + aux.winrate);
            }
        }
        public void display(Jugador[] j)
        {
            foreach(Jugador aux in j)
            {
                Console.WriteLine(aux.name + " - " + aux.nationality); 
            }
        }
        public void display(Element[] e)
        {
            foreach (Element aux in e)
            {
                Console.WriteLine(aux.jugador.nationality +" - " + aux.teamWinrate + " - " + aux.kda);
            }
        }


    }
}
