﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static eSportsLS.DataManager;

namespace eSportsLS.Functionalities
{
    class BucketSort
    {
        const int NUM_ALPHABET = 27 + 10 + 1;
        const int NUM_DIGITS = 10;
        // Create the queue for each bucket
        public List<Queue<eSports>> InitBuckets()
        {
            List<Queue<eSports>> buckets = new List<Queue<eSports>>();
            for (int i = 0; i < NUM_DIGITS; i++)
            {
                Queue<eSports> teams = new Queue<eSports>();
                buckets.Add(teams);
            }
            return buckets;
        }

        public eSports[] SortWinrate(eSports[] arr, List<Queue<eSports>> buckets, int posDigit = 0, bool sorted = false)
        {
            // Cas trivial, it's already sorted  
            if (sorted)
            {
                return arr;
            }
            else // Cas no trivial, anirem posant als buckets dependent del digit
            {
                // Per cada team, hem de mirar el digit i el coloquem al bucket corresponent
                for (int i = 0; i < arr.Length; i++)
                {
                    int digit = WhichBucket(arr[i].winrate, posDigit);
                    buckets[digit].Enqueue(arr[i]);
                }


                // Reconstruim l'array de equipos
                int i_arr = 0;
                int numFilledBuckets = 0;
                int numBuckets = buckets.Count;
                for (int i = numBuckets - 1; i >= 0; i--)
                {
                    int numItems = buckets[i].Count;
                    if (numItems > 0) numFilledBuckets++;
                    Queue<eSports> bucket = buckets[i];
                    while (bucket.Count > 0)
                    {
                        arr[i_arr++] = bucket.Dequeue();
                    }
                }

                return SortWinrate(arr, buckets, ++posDigit, numFilledBuckets == 1);
            }
        }

        private int WhichBucket(float winrate, int posDigit)
        {
            double bucketNumber = (winrate * 100) / Math.Pow(10, posDigit);
            bucketNumber = bucketNumber % 10;
            return (int)bucketNumber;
        }

        // Create the queue for each bucket for letters
        public List<List<Jugador>> InitBucketsLetters()
        {
            List<List<Jugador>> buckets = new List<List<Jugador>>();
            for (int i = 0; i < NUM_ALPHABET; i++)
            {
                List<Jugador> jugadors = new List<Jugador>();
                buckets.Add(jugadors);
            }
            return buckets;
        }

        public Jugador[] SortNationality(Jugador[] jugadors, List<List<Jugador>> buckets, int posLetter = 0, bool sorted = false)
        {
            if (sorted)
            {
                return jugadors;
            }
            else
            {
                // Per cada team, hem de mirar el digit i el coloquem al bucket corresponent
                for (int i = 0; i < jugadors.Length; i++)
                {
                    int digit = WhichBucket(jugadors[i].nationality, posLetter);
                    buckets[digit].Add(jugadors[i]);
                    // Ordenar los jugadores en cada bucket mirando siempre si la nacionalidad es igual para luego comprar el nombre
                    buckets[digit].Sort((x, y) => x.nationality.CompareTo(y.nationality) == 0 ?
                        string.Compare(x.name, y.name) : x.nationality.CompareTo(y.nationality));
                }


                // Reconstruim l'array de jugadors
                int i_jug = 0;
                int numFilledBuckets = 0;
                int numBuckets = buckets.Count;
                for (int i = 0; i < numBuckets; i++)
                {
                    int numItems = buckets[i].Count;
                    if (numItems > 0) numFilledBuckets++;
                    List<Jugador> bucket = buckets[i];
                    while (bucket.Count > 0)
                    {
                        jugadors[i_jug++] = bucket[0];
                        bucket.RemoveAt(0);
                    }
                }
                return SortNationality(jugadors, buckets, ++posLetter, numFilledBuckets == 1);
            }
        }

        private int WhichBucket(string nationality, int posLetter)
        {
            string national = nationality.ToLower();

            if (posLetter < national.Length)
            {
                if (national[national.Length - posLetter - 1] == 'ñ') return 'n' - 'a';
                else if (national[national.Length - posLetter - 1] == ' ') return NUM_ALPHABET - 1;
                else if (national[national.Length - posLetter - 1] >= '0' && national[national.Length - posLetter - 1] <= '9')
                    return NUM_ALPHABET - 1 - (national[national.Length - posLetter - 1] - '0');
                else return national[national.Length - posLetter - 1] - 'a';
            } else
            {
                return 0;
            }
        }

        public List<List<Element>> InitBucketsLettersPriority()
        {
            List<List<Element>> buckets = new List<List<Element>>();
            for (int i = 0; i < NUM_ALPHABET; i++)
            {
                List<Element> jugadors = new List<Element>();
                buckets.Add(jugadors);
            }
            return buckets;
        }

        public Element[] SortNationality(Element[] jugadors, List<List<Element>> buckets, int posLetter = 0, bool sorted = false)
        {
            if (sorted)
            {
                return jugadors;
            }
            else
            {
                // Per cada team, hem de mirar el digit i el coloquem al bucket corresponent
                for (int i = 0; i < jugadors.Length; i++)
                {
                    int digit = WhichBucket(jugadors[i].jugador.nationality, posLetter);
                    buckets[digit].Add(jugadors[i]);

                    /*Ordenamos los jugadores en cada bucket mirando si tienen la misma nacionalidad
                     * Y además si tienen el mismo teamwinrate miraremos su kda
                    */

                    buckets[digit].Sort((x, y) =>
                        x.jugador.nationality.CompareTo(y.jugador.nationality) == 0 ?
                        sameTeamWinrate(x.teamWinrate, y.teamWinrate) == 0 ? 
                        biggerKda(x.kda, y.kda) : sameTeamWinrate(x.teamWinrate, y.teamWinrate)
                        : x.jugador.nationality.CompareTo(y.jugador.nationality));

                    //buckets[digit].Sort((x, y) => x.nationality.CompareTo(y.nationality) == 0 ?
                      //  string.Compare(x.name, y.name) : x.nationality.CompareTo(y.nationality));
                }


                // Reconstruim l'array de jugadors
                int i_jug = 0;
                int numFilledBuckets = 0;
                int numBuckets = buckets.Count;
                for (int i = 0; i < numBuckets; i++)
                {
                    int numItems = buckets[i].Count;
                    if (numItems > 0) numFilledBuckets++;
                    List<Element> bucket = buckets[i];
                    while (bucket.Count > 0)
                    {
                        jugadors[i_jug++] = bucket[0];
                        bucket.RemoveAt(0);
                    }
                }
                return SortNationality(jugadors, buckets, ++posLetter, numFilledBuckets == 1);
            }
        }

        private static int sameTeamWinrate (float a1, float a2)
        {
            if (a1 == a2)
            {
                return 0;
            } else if (a1 < a2)
            {
                return 1;
            } else
            {
                return -1;
            }
        }

        private static int biggerKda (double a1, double a2)
        {
            if (a1 == a2)
            {
                return 0;
            }
            else if (a1 < a2)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        //----------------- Display Functions ------------------
        public void DisplaySortedWinrates(eSports[] e)
        {
            for (int i = 0; i < e.Length; i++)
            {
                Console.WriteLine(e[i].team + "-" + e[i].winrate);
            }
            Console.WriteLine();
        }

        public void DisplaySortedPlayers(Jugador[] j)
        {
            foreach (Jugador aux in j)
            {
                Console.WriteLine(aux.name + " - " + aux.nationality);
            }
        }

        public void DisplayPriorityPlayers (Element[] e)
        {
            foreach (Element aux in e)
            {
                Console.WriteLine(aux.jugador.nationality + " - " + aux.teamWinrate + " - " + aux.kda);
            }
        }
    }
}
