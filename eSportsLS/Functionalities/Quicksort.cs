﻿using System;
using System.Collections.Generic;
using System.Text;
using static eSportsLS.DataManager;

namespace eSportsLS.Functionalities
{
    class Quicksort
    {
        public eSports[] quickSort (eSports[] e, int left, int right)
        {
            int i = left, j = right;
            eSports pivot = e[(left + right) / 2];

            while (i <= j)
            {
                while (e[i].winrate > pivot.winrate) { i++; }
                while (e[j].winrate < pivot.winrate) { j--; }

                if (i < j)
                {
                    //Hacemos un swap entre los dos winrates
                    Swap(ref e[i], ref e[j]);
                    i++;
                    j--;
                }
                else { if (i == j) { i++; j--; } }
            }
            if (left < j) { quickSort(e, left, j); }
            if (i < right) { quickSort(e, i, right); }
            return e;
        }
        private static void Swap(ref eSports x, ref eSports y)
        {
            eSports temp = x;
            x = y;
            y = temp;
        }

        public Jugador[] quickSort(Jugador[] jugador, int left, int right)
        {
            int i = left, j = right;
            Jugador pivot = jugador[(left + right) / 2];

            while (i <= j)
            {
                while(esMenor(jugador[i],pivot)) { i++; }
                while(esMayor(jugador[j],pivot)) { j--; }

                if (i < j)
                {
                    Swap(ref jugador[i], ref jugador[j]);
                    i++;
                    j--;
                }
                else
                {
                   
                    if (i==j)
                    {
                        i++;
                        j--;
                    }
                }
            }
            if (left < j) { quickSort(jugador, left, j); }
            if (i < right) { quickSort(jugador, i, right); }
            return jugador;
        }

        private static bool esMenor (Jugador j1, Jugador j2)
        {
            if (j1.nationality.CompareTo(j2.nationality) < 0)
            {
                return true;
            }
            else if (j1.nationality.CompareTo(j2.nationality) == 0)
            {
                if (j1.name.CompareTo(j2.name) < 0)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool esMayor(Jugador j1, Jugador j2)
        {
            if (j1.nationality.CompareTo(j2.nationality) > 0)
            {
                return true;
            }
            else if (j1.nationality.CompareTo(j2.nationality) == 0)
            {
                if (j1.name.CompareTo(j2.name) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        private static void Swap(ref Jugador x, ref Jugador y)
        {
            Jugador temp = x;
            x = y;
            y = temp;
        }

        private static void Swap(ref Element x, ref Element y)
        {
            Element temp = x;
            x = y;
            y = temp;
        }

        public Element[] quickSortPlayers(Element[] e, int left, int right)
        {
            int i = left, j = right;
            Element pivot = e[(left + right) / 2];

            while (i <= j)
            {
                while (esMenorPrioridad(e[i], pivot)) { i++; }
                while (esMayorPrioridad(e[j], pivot)) { j--; }

                if (i < j)
                {
                    Swap(ref e[i], ref e[j]);
                    i++;
                    j--;
                }
                else
                {

                    if (i == j)
                    {
                        i++;
                        j--;
                    }
                }
            }
            if (left < j) { quickSortPlayers(e, left, j); }
            if (i < right) { quickSortPlayers(e, i, right); }
            return e;
        }

        private static bool esMenorPrioridad(Element j1, Element j2)
        {
            if (j1.jugador.nationality.CompareTo(j2.jugador.nationality) < 0)
            {
                return true;
            }
            else if (j1.jugador.nationality.CompareTo(j2.jugador.nationality) == 0)
            {
                if (j1.teamWinrate > j2.teamWinrate)
                {
                    return true;
                }
                else if (j1.teamWinrate == j2.teamWinrate)
                {
                    if (j1.kda > j2.kda) { return true; }
                }

            }
            return false;
        }

        private static bool esMayorPrioridad(Element j1, Element j2)
        {
            if (j1.jugador.nationality.CompareTo(j2.jugador.nationality) > 0)
            {
                return true;
            }
            else if (j1.jugador.nationality.CompareTo(j2.jugador.nationality) == 0)
            {
                if (j1.teamWinrate < j2.teamWinrate)
                {
                    return true;
                }
                else if (j1.teamWinrate == j2.teamWinrate)
                {
                    if (j1.kda < j2.kda) { return true; }
                }

            }
            return false;
        }

        // Display
        public void display (eSports[] e)
        {
            foreach (eSports aux in e)
            {
                Console.WriteLine("Team: " + aux.team + " - " + aux.winrate);
            }
        }

        public void display(Jugador[] j)
        {
            foreach(Jugador aux in j)
            {
                Console.WriteLine(aux.name + " - " + aux.nationality);
            }
        }

        public void display(Element[] j)
        {
            foreach (Element aux in j)
            {
                Console.WriteLine(aux.jugador.nationality + " - " + aux.teamWinrate + " - " + aux.kda);
            }

        }
    }
}
