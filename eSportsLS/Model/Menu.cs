﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS
{
    class Menu
    {
        private static int MAX = 5;
        private static int MIN = 1;
        private int option1;
        private int option2;


        public void showMenuMethods()
        {
            Console.WriteLine("Welcome to eSportsLS\n");
            Console.WriteLine("1. Bucket Sort\n2. Merge Sort\n3. Quick Sort\n4. Radix Sort\n5. Exit\n\nOption: ");
        }

        public void showMenuFunctionality(int type)
        {
            Console.WriteLine("Ordenar por: ");
            switch (type)
            {
                case 4:
                    Console.WriteLine("1. Winrate\n2. Combinación de prioridades");
                    break;
                default:
                    Console.WriteLine("1. Winrate\n2. Nacionalidad\n3. Combinación de prioridades");
                    break;
            }
        }

        public int getOption1() { return option1; }
        public int getOption2() { return option2; }
        public bool validOption1() { return option1 >= MIN && option1 <= MAX; }
        public bool validOption2(int type) 
        { 
            switch (type)
            {
                case 4:
                    return option2 >= 1 && option2 <= 2;
                default:
                    return option2 >= MIN && option2 <= 3;
            }
        }
        public void optionInput1() { option1 = Convert.ToInt32(Console.ReadLine()); }
        public void optionInput2() { option2 = Convert.ToInt32(Console.ReadLine()); }
        public bool isExit() { return option1 == MAX; }

    }
}
