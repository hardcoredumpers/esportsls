﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS
{
    class eSports
    {
        public String team { get; set; }
        public float winrate { get; set; }
        public Jugador[] jugadores { get; set; }
    }
}
