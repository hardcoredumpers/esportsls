﻿using eSportsLS.Functionalities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace eSportsLS
{
    class DataManager
    {
        public struct Element
        {
            public float teamWinrate;
            public Jugador jugador;
            public double kda;
        }
        public ArrayList arrayList;
        public eSports[] esportsls;
        public Jugador[] jugadores;
        public Quicksort qs;
        public MergeSort ms;
        public RadixSort rs;
        public BucketSort bs;
        public Element[] jugadoresPrioridad;

        public DataManager()
        {
            initializeMethods();
            
        }

        private void initializeMethods()
        {
            qs = new Quicksort();
            ms = new MergeSort();
            rs = new RadixSort();
            bs = new BucketSort();

        }

        public ArrayList getJson() { return arrayList; }
        public void readJSON(string jsonFile)
        {
            using (StreamReader r = new StreamReader(jsonFile))
            {
                string json = r.ReadToEnd();
                var esports = JsonConvert.DeserializeObject<List<eSports>>(json);
                arrayList = new ArrayList();

                foreach (var data in esports) {
                    arrayList.Add(data);
                }
                esportsls = arrayList.ToArray(typeof(eSports)) as eSports[];
            }

            ArrayList arrayListAux = new ArrayList();
            foreach (eSports e in esportsls)
            {
                foreach(Jugador j in e.jugadores)
                {
                    j.name = char.ToUpper(j.name[0]) + j.name.Substring(1);
                    j.nationality = char.ToUpper(j.nationality[0]) + j.nationality.Substring(1);
                    arrayListAux.Add(j);
                }
            }
            jugadores = arrayListAux.ToArray(typeof(Jugador)) as Jugador[];

            //Guardamos los datos de los jugadores junto con el winrate de su equipo
            jugadoresPrioridad = new Element[jugadores.Length];
            int i = 0;

            foreach (eSports eAux in esportsls)
            {
                
                    foreach (Jugador jAux in eAux.jugadores)
                    {
                        jugadoresPrioridad[i].teamWinrate = eAux.winrate;
                        jugadoresPrioridad[i].jugador = jAux;
                        jugadoresPrioridad[i].kda = kdaCalculator(jAux);
                        i++;
                    }
                
            }
        }
        private static double kdaCalculator (Jugador j)
        {
            double kda = (j.total_kills + j.total_assistances * 0.5) / j.total_deaths;
            return kda;
        }

        public void start(int option1, int option2)
        {
            switch(option1)
            {
                //BucketSort
                case 1:
                    if (option2 == 1)
                    {
                        List<Queue<eSports>> buckets = bs.InitBuckets();
                        eSports[] sorted = bs.SortWinrate(esportsls, buckets);
                        bs.DisplaySortedWinrates(sorted);
                    }
                    if (option2 == 2)
                    {
                        List<List<Jugador>> bucketsJugadors = bs.InitBucketsLetters();
                        Jugador[] sortedJugadors = bs.SortNationality(jugadores, bucketsJugadors);
                        bs.DisplaySortedPlayers(sortedJugadors);
                    }
                    if (option2 == 3)
                    {
                        List<List<Element>> bucketsPriority = bs.InitBucketsLettersPriority();
                        Element[] sortedPlayers = bs.SortNationality(jugadoresPrioridad, bucketsPriority);
                        bs.DisplayPriorityPlayers(sortedPlayers);

                    }
                    break;
                //MergeSort
                case 2:
                    if (option2 == 1) 
                    { 
                        eSports[] sorted = ms.mergeSort(esportsls, 0, esportsls.Length -1); 
                        ms.display(sorted); 
                    }
                    if (option2 == 2) 
                    { 
                        Jugador[] sorted = ms.mergeSortNationality(jugadores, 0, jugadores.Length - 1);
                        ms.display(sorted); 
                    }
                    if (option2 == 3) 
                    {
                        //Ordenamos la lista por nacionalidad
                        //Element[] sortNationalityFirst = ms.mergeSortAux(jugadoresPrioridad, 0, jugadoresPrioridad.Length - 1);
                        Element[] sorted = ms.mergeSortPlayers(jugadoresPrioridad, 0, jugadoresPrioridad.Length - 1); 
                        ms.display(sorted); 
                    }
                    break;
                //QuickSort
                case 3:
                    if (option2 == 1) 
                    { 
                        eSports[] sorted = qs.quickSort(esportsls, 0, esportsls.Length - 1); 
                        qs.display(sorted); 
                    }
                    if (option2 == 2) 
                    { 
                        Jugador[] sorted = qs.quickSort(jugadores, 0, jugadores.Length - 1); 
                        qs.display(sorted); 
                    }
                    if (option2 == 3)
                    {
                        Element[] sorted = qs.quickSortPlayers(jugadoresPrioridad, 0, jugadoresPrioridad.Length - 1);
                        qs.display(sorted);
                    }
                    break;
                //RadixSort
                case 4:
                    if (option2 == 1)
                    {
                        List<ArrayList> bucketsRadix = rs.InitBuckets();
                        eSports[] sortedRadix = rs.SortWinrate(esportsls, bucketsRadix);
                        //eSports[] sortedRadix = rs.SortWinrate(esportsls);
                        rs.DisplaySortedWinrates(sortedRadix);
                    }
                    if (option2 == 2)
                    {
                        List<List<Element>> buckets = rs.InitBucketsElements();
                        Element[] sorted = rs.SortPriorities(jugadoresPrioridad, buckets);
                        rs.DisplaySortedPlayers(sorted);
                    }
                    break;
            }
        }
    }
}
