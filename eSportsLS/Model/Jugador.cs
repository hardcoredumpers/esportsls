﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSportsLS
{
    class Jugador
    {
        public String name { get; set; }
        public int total_kills { get; set; }
        public int total_deaths { get; set; }
        public int total_assistances { get; set; }
  
        public String nationality { get; set; }
        public String main_position { get; set; }
        public Legend[] main_legends { get; set; }


    }
}
